const path = require('path');
const isProduction = process.env.NODE_ENV === 'production';

const config = {
    devServer: { inline: true },
    devtool: !isProduction ? 'source-map' : false,
    context: __dirname,
    entry: {
        main: './index.js',
    },
    output: {
        path: path.join(__dirname, './build/js'),
        filename: '[name].js',
    },
    stats: {
        colors: true,
        timings: true
    },
    resolve: {
        extensions: ['.js', '.scss', 'css'],
        modules: ['./node_modules', path.resolve('./app')],
        alias: {
            pure: path.join(__dirname, '/node_modules/purecss/build/pure.css'),
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-2'],
                },
            }, {
                test: /\.css/,
                loaders: [
                    'style-loader',
                    `css-loader?${JSON.stringify({
                        sourceMap: !isProduction,
                        modules: true,
                        localIdentName: !isProduction ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',
                        minimize: isProduction,
                        camelCase: true,
                    })}`,
                ],
            }, {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader',
                }, {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                    },
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true,
                    },
                }],
            },
        ],
    },
};

module.exports = config;
