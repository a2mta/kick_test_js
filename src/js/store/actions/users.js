export const LOADING_USERS = 'LOADING_USERS';
export const USERS_LOADED = 'USERS_LOADED';
export const SWITCH_USER_STATUS = 'SWITCH_USER_STATUS';
export const SEND_DATA = 'SEND_DATA';
export const DELETE_USERS = 'DELETE_USERS';
export const ADD_USER = 'ADD_USER';

export function usersLoaded(data) {
    return {
        type: USERS_LOADED,
        data
    };
}

export function deleteUsers() {
    return {
        type: DELETE_USERS
    };
}

export function siwthcUserStatus(status, userId) {
    return {
        type: SWITCH_USER_STATUS,
        userId,
        status
    };
}

export function sendData(userId, status) {
    return {
        type: SEND_DATA,
        userId,
        status
    };
}


export function addUser(name, email) {
    const newUser = { status: false, email: email, name: name, id: Math.floor(Math.random() * 101) };
    return {
        type: ADD_USER,
        newUser
    };
}

export function loadingUsers() {
    return {
        type: LOADING_USERS
    };
}

export function loadUsers() {
    return (dispatch) => {
        dispatch(loadingUsers());
        fetch('https://5b3b45b2e7659e001496947c.mockapi.io/api/db/users',
            {
                method: 'GET',
                mode: 'cors',
                cache: 'default'
            })
            .then(response => response.json())
            .then((users) => {
                dispatch(usersLoaded(users));
            })
            .catch(error => console.error(error));
    };
}