import { USERS_LOADED, LOADING_USERS, SWITCH_USER_STATUS, ADD_USER, SEND_DATA, DELETE_USERS } from '../actions/users';

function init() {
    return {
        status: 0
    }
}

export default function data(state = init(), action) {
    switch (action.type) {
        case SWITCH_USER_STATUS:
            return {
                ...state,
                list: state.list.map((item) => {
                    if (item.id !== action.userId) {
                        return item;
                    }
                    return {
                        ...item,
                        checked: action.status
                    };
                })
            };
        case DELETE_USERS:
            return {
                ...state,
                list: state.list.filter((item) => {
                    return !item.checked;
                })
            };
        case SEND_DATA:
            return {
                ...state,
                list: state.list.map((item) => {
                    if (item.id !== action.userId) {
                        return item;
                    }
                    return {
                        ...item,
                        status: action.status
                    };
                })
            };
        case ADD_USER:
            const newList = state.list;
            newList.push(action.newUser);
            return {
                ...state,
                list: newList,
                status: 1
            };
        case LOADING_USERS:
            return {
                ...state,
                status: 1
            };
        case USERS_LOADED:
            return {
                ...state,
                list: action.data,
                status: 0
            };
        default:
            return state;
    }
}
