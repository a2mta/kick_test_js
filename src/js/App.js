
import React, { Component } from 'react';
import ContentContainer from './containers/ContentContainer';
import '../style/index.scss'

class App extends Component {
    render() {
        return (
            <ContentContainer />
        );
    }
}

export default App;
