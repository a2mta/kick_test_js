import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import Checkbox from './Checkbox';


class Content extends Component {

    constructor() {
        super();
        this.state = {
            typeFilter: null
        };
        this.switchFilter = this.switchFilter.bind(this);
        this.addUser = this.addUser.bind(this);
        this.checkValidity = this.checkValidity.bind(this);
        this.userEmail = React.createRef();
        this.userName = React.createRef();
    }

    componentDidMount() {
        this.props.loadUsers();
    }

    checkValidity() {
        const userName = this.userName.current;
        const userEmail = this.userEmail.current;
        if (!userEmail.checkValidity()) {
            userEmail.classList.add('error');
            return false;
        }
        else {
            userEmail.classList.remove('error');
        }
        if (!userName.checkValidity()) {
            userName.classList.add('error');
            return false;
        }
        else {
            userName.classList.remove('error');
        }
        return true;
    }

    addUser() {
        if (this.checkValidity()) {
            this.props.addUser(this.userName.current.value, this.userEmail.current.value);
            this.userName.current.value = '';
            this.userEmail.current.value = '';
        }
    }

    switchFilter(status) {
        this.setState({ typeFilter: status });
    }

    render() {
        const { typeFilter } = this.state;
        const users = this.props.users.list ? this.props.users.list.filter((user) => { return typeFilter === null ? true : user.status === typeFilter; }) : [];
        return (
            <div className="content-wrapper">
                <div className="users-add-form pure-form pure-form-stacked">
                    <fieldset>
                        <legend>Add recipient</legend>
                        <div className="users-form-input">
                            <label htmlFor="email">Email</label>
                            <input ref={this.userEmail} required id="email" type="email" placeholder="johndoe@gmail.com" />
                        </div>
                        <div className="users-form-input">
                            <label htmlFor="name">Name</label>
                            <input ref={this.userName} required id="name" type="text" placeholder="John Doe" />
                        </div>
                        <button className="pure-button button button-rounded button-big" onClick={() => this.addUser()}> Add email </button>
                    </fieldset>
                </div>
                <div className="users-table-tabs">
                    <div className="tabs-left">
                        <div className={`tab ${typeFilter === null ? 'active' : ''}`} onClick={() => this.switchFilter(null)} >Show all</div>
                        <div className={`tab ${typeFilter ? 'active' : ''}`} onClick={() => this.switchFilter(true)} >Show sent</div>
                        <div className={`tab ${typeFilter === false ? 'active' : ''}`} onClick={() => this.switchFilter(false)} >Show unset</div>
                    </div>
                    <div className="tabs-right">
                        <span onClick={() => { this.props.deleteUsers(); }}>
                            Delete selected recipients
                        </span>
                    </div>
                </div>
                <table className="users-table pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th />
                            <th>Email</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
                <Scrollbars style={{ height: 140 }} autoHide autoHideTimeout={1000} autoHideDuration={200}>
                    <table className="users-table pure-table pure-table-horizontal">
                        <tbody>
                            {users.map((user, index) =>

                                <tr key={user.id}>
                                    <td>
                                        <Checkbox id={user.id} checked={user.checked} handleClick={(status) => this.props.siwthcUserStatus(status, user.id)} />
                                    </td>
                                    <td>{user.email}</td>
                                    <td>{user.name}</td>
                                    <td>{user.status ? 'Sent' : 'Unsent'}</td>
                                    <td> {user.status ? null : <button type="button" className="pure-button button button-rounded" onClick={() => { this.props.sendData(user.id, !user.status); }} > {user.status ? 'Cancel' : 'Send'} </button>}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </Scrollbars>
            </div >
        );
    }
}


export default Content;