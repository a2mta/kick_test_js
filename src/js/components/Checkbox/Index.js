import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class Checkbox extends Component {

    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(status) {
        this.props.handleClick(status);
    }

    render() {
        const { checked, id } = this.props;
        return (
            <label htmlFor={`user${id}_status`} className="square_checkbox">
                <input id={`user${id}_status`} type="checkbox" onChange={() => this.handleClick(!checked)} checked={checked} />
                <span className="checkmark" />
            </label>
        );
    }
}

Checkbox.propTypes = {
    checked: PropTypes.bool,
    id: PropTypes.string
};

export default Checkbox;