import { connect } from 'react-redux';
import Content from '../components/Content';
import { loadUsers, siwthcUserStatus, addUser, sendData, deleteUsers } from '../store/actions/users';

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    };
}

export default connect(mapStateToProps, {
    loadUsers,
    addUser,
    siwthcUserStatus,
    deleteUsers,
    sendData
})(Content);
