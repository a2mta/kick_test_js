import React from "react";
import ReactDOM from "react-dom";
import App from './src/js/App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './src/js/store/reducers/index';

const loggerMiddleware = createLogger()


const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
)

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById("root"));